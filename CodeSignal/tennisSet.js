/*
    v > 7 | s == v ? false :
    s == 6 & v < 5 | v == 6 & s < 5 ? true :        
    s == 7 & v > 4 | v == 7 & s > 4
*/

tennisSet = (s, v) => s != v & (s<v?v:s) == ( (s>v?v:s) < 5 ?  6 : 7 )     
    
/*
    (s < v ? v : s) => Math.max()
    (s > v ? v : s) => Math.min()
*/