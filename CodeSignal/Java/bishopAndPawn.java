boolean bishopAndPawn(String bishop, String pawn) {

    char[] b = bishop.toCharArray();
    char[] p = pawn.toCharArray();
     
    return Math.abs(b[0]-p[0]) == Math.abs(b[1]-p[1]);
}

https://codefights.com/tournaments/r5rGu3aXsqFRvAf2S/C
