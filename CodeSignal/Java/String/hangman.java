boolean hangman(String w, String l) {
    int c = 0, i = 0;
    String s = "";
    for (char ch : w.toCharArray()) {
        if (s.indexOf(ch) < 0) s += ch;
    }

    for (char ch: l.toCharArray()) {
        if (w.indexOf(ch) < 0) c++;
        else i++;
        if (c >= 6) return false;
        if (i == s.length()) return true;
    }

    return false;
}
