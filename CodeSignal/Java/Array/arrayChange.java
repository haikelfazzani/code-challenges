int arrayChange(int[] inputArray) {
        int result = 0;
        for (int i = 1; i < inputArray.length; i++) {
                   if (inputArray[i] <= inputArray[i – 1]) {
                             result += inputArray[i – 1] – inputArray[i] + 1;
                             inputArray[i] = inputArray[i-1] + 1;
                    }
        }
        return result;
}

/*
You are given an array of integers. On each move you are allowed to increase exactly one of its element by one. 
Find the minimal number of moves required to obtain a strictly increasing sequence from the input.

Example

For inputArray = [1, 1, 1], the output should be
arrayChange(inputArray) = 3.
*/

Another solution :

int arrayChange(int[] inputArray) {
       int last = inputArray[0];
       int count = 0;
       for (int i = 1; i < inputArray.length; i++) {
                while (last >= inputArray[i]) {
                           ++count;
                           ++inputArray[i];
                }
       last = inputArray[i];
       }
       return count;
}
