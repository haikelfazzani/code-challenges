String alphabeticShift(String inputString) {
  StringBuilder chars = new StringBuilder();
  for (int i = 0; i < inputString.length(); ++i) {
    int number = inputString.charAt(i) - 'a';
    number = (number + 1) % 26;
    chars.append((char) number);
  }
  return chars.toString();
}
/*
Given a string, replace each its character by the next one in the English alphabet (z would be replaced by a).

For inputString = "crazy", the output should be
alphabeticShift(inputString) = "dsbaz".
*/
