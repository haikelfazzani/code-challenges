String buildPalindrome(String st) {
    if(palindrome(st)) return st;
    String r = s;
    for(int i= 0; i < st.length()-1; i++){ 
        String found = r; 
            for(int j = i; j >= 0; j--){
                found += st.charAt(j);
            }
        if(palindrome(found)) return found;
    }
    return r;   
}

boolean palindrome(String st){
    int len = st.length();
    for(int i = 0 ; i < st.length()/2 ;i++){
        if(st.charAt(i) != st.charAt(len-1-i))
            return false;
    }
    return true;
}

/*
Given a string, find the shortest possible string which can be achieved by adding characters 
to the end of initial string to make it a palindrome.

For st = "abcdc", the output should be
buildPalindrome(st) = "abcdcba".
*/
