String[] addBorder(String[] picture) {
String [] s= new String [picture.length+2];
    String temp="";
    for(int i=0;i<picture[0].length()+2;i++){
        temp=temp+"*";
    }
    s[0]=temp;
    s[s.length-1]=temp; int cnt=1;
    for(int j=0;j<picture.length;j++){
        temp="*"+picture[j]+"*";
        s[cnt]=temp;
        cnt++;
    } return s;
}

/*
Given a rectangular matrix of characters, add a border of asterisks(*) to it.

Example

For

picture = ["abc",
           "ded"]
the output should be

addBorder(picture) = ["*****",
                      "*abc*",
                      "*ded*",
                      "*****"]
*/
