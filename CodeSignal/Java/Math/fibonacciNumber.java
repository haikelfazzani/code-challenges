int fibonacciNumber(int n) {
        if (n == 0 || n==1) return n;
        return fibonacciNumber(n - 1) + fibonacciNumber(n - 2);
}
/*
Given integer index n, find the nth Fibonacci number.
Example
For n = 2, the output should be
fibonacciNumber(n) = 1.
F2 = F0 + F1 = 0 + 1 = 1
*/
