boolean almostIncreasingSequence(int[] a) {
    int cnt = 0 , cnt2 = 0;
    for(int i = 0 ; i < a.length-1 ; i++){
        if(a[i] >= a[i+1]) cnt++;
    }
    for(int i = 0 ; i < a.length-2 ; i++){
        if(a[i] >= a[i+2]) cnt2++;
    }
    return cnt + cnt2 > 2 ? false : true;
}

/*
Given a sequence of integers as an array, 
determine whether it is possible to obtain a strictly increasing sequence by removing no more than one element from the array.

For sequence = [1, 3, 2, 1], the output should be
almostIncreasingSequence(sequence) = false;

There is no one element in this array that can be removed in order to get a strictly increasing sequence.

For sequence = [1, 3, 2], the output should be
almostIncreasingSequence(sequence) = true.

You can remove 3 from the array to get the strictly increasing sequence [1, 2]. Alternately, 
you can remove 2 to get the strictly increasing sequence [1, 3].
*/
