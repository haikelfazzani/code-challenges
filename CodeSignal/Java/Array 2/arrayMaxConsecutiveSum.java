int arrayMaxConsecutiveSum(int[] a, int k) {
    int sum = 0, max = -1 * Integer.MAX_VALUE;
    for(int i = 0; i < k; i++) sum += a[i]; 
    
    if(sum > max) max = sum;
    
    for(int i = k; i < a.length; i++){ 
        sum = sum + (a[i] - a[i-k]); 
        if(sum > max) max = sum;
    }
    return max;
}

Another Solution :

int arrayMaxConsecutiveSum(int[] a, int k) {
   int max = 0;
   for (int i = 0; i < a.length - k + 1; i++) {
   int sum = 0;
    for (int j = 0; j < k; j++) {
      sum += a[i + j];
    }
    max = Math.max(max, sum);
  }
  return max; 
}

/*
Given array of integers, find the maximal possible sum of some of its k consecutive elements.

For inputArray = [2, 3, 5, 1, 6] and k = 2, the output should be
arrayMaxConsecutiveSum(inputArray, k) = 8.
All possible sums of 2 consecutive elements are:

2 + 3 = 5;
3 + 5 = 8;
5 + 1 = 6;
1 + 6 = 7.
Thus, the answer is 8.
*/
