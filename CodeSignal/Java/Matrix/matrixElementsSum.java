int matrixElementsSum(int[][] matrix) {
      int sum =0;
        for(int i =0; i<matrix[0].length;i++) {
            for(int j=0; j<matrix.length;j++) {
                if(matrix[j][i] > 0)
                    sum +=matrix[j][i];
                else
                    break;
            }
        }
        return sum;
}
/*
Example
For
matrix = [[0, 1, 1, 2], 
          [0, 5, 0, 0], 
          [2, 0, 3, 3]]
the output should be
matrixElementsSum(matrix) = 9.
 the rooms matrix with unsuitable rooms marked with 'x':
 [[x, 1, 1, 2],
 [x, 5, x, x],
 [x, x, x, x]]
Output : 1 + 1 + 2 + 5 = 9.
*/
