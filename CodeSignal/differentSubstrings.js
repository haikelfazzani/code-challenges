differentSubstrings = s => {

    let a = new Set(), i, j
    
    for(i in s)
        for(j = i ; j < s.length ;)             
            a.add(s.slice(i,++j))                                        
    
    return a.size
}

/*
    https://app.codesignal.com/challenge/bGtXGtqgpXXAYngA6

    Given a string, find the number of different non-empty substrings in it.

    Example

    For inputString = "abac", the output should be
    differentSubstrings(inputString) = 9.
    They are:

    "a", "b", "c",
    "ab", "ac", "ba",
    "aba", "bac",
    "abac"

    Solution :
*/