/*
Given a binary array, find the maximum number of consecutive 1s in this array.

Input: [1,1,0,1,1,1]
Output: 3
Explanation: The first two digits or the last three digits are consecutive 1s.
    The maximum number of consecutive 1s is 3.
*/

public int findMaxConsecutiveOnes(int[] a) {
  int max = 0 , cnt = 0;
  for(int i = 0 ; i < a.length ;i++){
    if(a[i] == 1){
      cnt++; // First 2 : Second 3 
      max = Math.max(max , cnt); // First iteration max 2 : Second iteration max = 3
    }
    else cnt = 0; // if a[i] != 1 ; initial cnt to 0 And check again 
  }
  return max;
}