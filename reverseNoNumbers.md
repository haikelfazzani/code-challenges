### Description
Today you have to reverse the string but without the index of the numbers.

Example: "he2llo" - "ol2leh"  
Example: "De5ary4ou" - "uo5yra4eD"  
Example: "D23amn5Boy" - "y23oBn5maD"  

### Solution
```js
reverseNoNumbers = (a) => (       
    v = a.match(/\D/g),
    a.split``.map(i => /\D/.test(i) ? v.pop`` : i).join``
)
```